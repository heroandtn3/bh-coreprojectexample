package com.bh.coreproject.example.client;

import com.google.gwt.core.client.GWT;

public class ClientToken {
	public static final String NODE_JS_URL_ROOT = !GWT.isScript() ? "http://localhost:3002" : "https://sfcfb.com:3002";
	public static final String NODE_JS_URL = NODE_JS_URL_ROOT + "/socket.io/socket.io.js";
	public static final String packageName = "com.koolsoft.game.chess";
	public static final String APP_NAME = packageName;
}

/*
 * Copyright 2014 heroandtn3 (heroandtn3 [at] gmail.com) 
 * 
 * This file is part of CoreProjectExample.
 * CoreProjectExample is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * CoreProjectExample is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with CoreProjectExample.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 */
package com.bh.coreproject.example.client.activities.home;

import com.bh.coreproject.example.client.ClientFactory;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.th.core.client.activities.basic.BasicActivity;

/**
 * @author heroandtn3
 *
 */
public class HomeActivity extends BasicActivity<ClientFactory>{

	private HomeView view;

	/**
	 * 
	 */
	public HomeActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}

	@Override
	protected void onBackButtonPressed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onHomeButtonPressed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getHomeView();
		bind(view);
		panel.setWidget(view.asWidget());
		
		
	}

}

/*
 * Copyright 2014 heroandtn3 (heroandtn3 [at] gmail.com) 
 * 
 * This file is part of CoreProjectExample.
 * CoreProjectExample is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * CoreProjectExample is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with CoreProjectExample.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 */
package com.bh.coreproject.example.client.activities.login;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasText;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.googlecode.mgwt.ui.client.widget.MTextBox;
import com.googlecode.mgwt.ui.client.widget.WidgetList;
import com.th.core.client.activities.basic.BasicViewGwtImpl;

/**
 * @author heroandtn3
 *
 */
public class LoginViewMgwt extends BasicViewGwtImpl implements LoginView {
	
	private HTML title;
	private MTextBox name;
	private Button startBtn;

	/**
	 * 
	 */
	public LoginViewMgwt() {
		WidgetList container = new WidgetList();
		container.setRound(true);
		scrollPanel.add(container);
		
		title = new HTML();
		title.addStyleName("title");
		container.add(title);
		title.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		
		name = new MTextBox();
		name.setPlaceHolder("Enter your name");
		container.add(name);
		
		startBtn = new Button("Start");
		startBtn.setConfirm(true);
		container.add(startBtn);
		
	}

	@Override
	public HasTapHandlers getStartBtn() {
		return startBtn;
	}
	
	@Override
	public HasText getName() {
		return name;
	}
	
	@Override
	public HasText getTitle() {
		return title;
	}

}

/*
 * Copyright 2014 heroandtn3 (heroandtn3 [at] gmail.com) 
 * 
 * This file is part of CoreProjectExample.
 * CoreProjectExample is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * CoreProjectExample is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with CoreProjectExample.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 */
package com.bh.coreproject.example.client.activities.login;

import com.bh.coreproject.example.client.ClientFactory;
import com.bh.coreproject.example.client.ClientToken;
import com.bh.coreproject.example.client.activities.home.HomePlace;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.th.core.client.activities.basic.BasicActivity;
import com.th.core.client.event.LoginEvent;
import com.th.core.client.multigame.Player;

/**
 * @author heroandtn3
 *
 */
public class LoginActivity extends BasicActivity<ClientFactory> {

	private LoginView view;

	public LoginActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}

	@Override
	protected void onBackButtonPressed() {
		goTo(new HomePlace());
	}

	@Override
	protected void onHomeButtonPressed() {
		goTo(new HomePlace());
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getLoginView();
		bind(view);
		panel.setWidget(view.asWidget());
		
		view.getHeaderBackButton().setText("Logout");
		
		view.getTitle().setText("Welcome to demo chatting rom");
		
		addHandlerRegistration(view.getStartBtn().addTapHandler(new TapHandler() {
			
			@Override
			public void onTap(TapEvent event) {
				String name = view.getName().getText();
				if (name.length() != 0) {
					clientFactory.getEventBus().fireEvent(new LoginEvent(true));
					Cookies.setCookie(ClientToken.APP_NAME, name);
					clientFactory.getMultigameController().initialize(new Player(name, ClientToken.APP_NAME));
					goTo(new HomePlace());
				} else {
					view.alert("", "Please enter your name", null);
				}
			}
		}));
	}
}

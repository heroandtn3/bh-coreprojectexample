package com.bh.coreproject.example.client;

import com.bh.coreproject.example.client.activities.AppPlaceHistoryMapper;
import com.bh.coreproject.example.client.activities.PhoneActivityMapper;
import com.bh.coreproject.example.client.activities.PhoneAnimationMapper;
import com.bh.coreproject.example.client.activities.login.LoginPlace;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.shared.GWT;
import com.th.core.client.BasicEntry;

public class MgwtAppEntryPoint extends BasicEntry<ClientFactory> implements EntryPoint {

	@Override
	public void onModuleLoad() {
		ClientFactory clientFactory = new ClientFactoryImpl();
		super.onModuleLoad(clientFactory, clientFactory.getMultigameController());
	}

	@Override
	public void startApp() {
		AppPlaceHistoryMapper historyMapper = GWT.create(AppPlaceHistoryMapper.class);
		super.startApp(
				historyMapper, 
				new PhoneActivityMapper(getClientFactory()), 
				new PhoneAnimationMapper(), 
				new LoginPlace());
		multiGameController.setting(ClientToken.NODE_JS_URL_ROOT, ClientToken.NODE_JS_URL, ClientToken.APP_NAME);
	}

}

/*
 * Copyright 2013 heroandtn3 (heroandtn3 [at] gmail.com - www.sangnd.com
 * )
 * This file is part of mFaceme.
 * mFaceme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * mFaceme is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with mFaceme.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.bh.coreproject.example.client.activities;

import com.bh.coreproject.example.client.ClientFactory;
import com.bh.coreproject.example.client.activities.home.HomeActivity;
import com.bh.coreproject.example.client.activities.home.HomePlace;
import com.bh.coreproject.example.client.activities.login.LoginActivity;
import com.bh.coreproject.example.client.activities.login.LoginPlace;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.place.shared.Place;
import com.th.core.client.activities.BasicPhoneActivityMapper;

/**
 * 
 */
public class PhoneActivityMapper extends BasicPhoneActivityMapper<ClientFactory> {

	public PhoneActivityMapper(ClientFactory clientFactory) {
		super(clientFactory);
	}

	@Override
	public Activity getActivity(Place place) {
		if (place instanceof HomePlace) {
			return new HomeActivity(clientFactory, place);
		}
		
		if (place instanceof LoginPlace) {
			return new LoginActivity(clientFactory, place);
		}
		return null;
	}
}

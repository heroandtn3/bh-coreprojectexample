/*
 * Copyright 2014 heroandtn3 (heroandtn3 [at] gmail.com) 
 * 
 * This file is part of CoreProjectExample.
 * CoreProjectExample is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * CoreProjectExample is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with CoreProjectExample.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 
 */
package com.bh.coreproject.example.client;

import com.bh.coreproject.example.client.activities.home.HomeView;
import com.bh.coreproject.example.client.activities.login.LoginView;
import com.th.core.client.activities.BasicClientFactory;

/**
 * @author heroandtn3
 *
 */
public interface ClientFactory extends BasicClientFactory {

	HomeView getHomeView();
	LoginView getLoginView();
	AppMultigameController getMultigameController();
}
